<?php
declare(strict_types=1);

namespace markg\lang;

class Lang extends \think\Lang
{
    const LANG_VERSION = '1';

    /**
     * configuration parameter
     * @var array
     */
    protected $config = [
        // default language
        'default_lang'    => 'zh-cn',
        // Default country
        'default_country' => '',
        // List of allowed languages
        'allow_lang_list' => [],
        // Whether to use cookies to record
        'use_cookie'      => true,
        // Extended Language Pack
        'extend_list'     => [],
        // Multilingual cookie variables
        'cookie_var'      => 'think_lang',
        // Multilingual header variables
        'header_var'      => 'think-lang',
        // Multilingual automatic detection of variable names
        'detect_var'      => 'lang',
        // Translating Accept-Language into corresponding language pack names
        'accept_language' => [],
        // Does it support language grouping
        'allow_group'     => false,
        // Language version
        'lang_version'    => self::LANG_VERSION
    ];

    /** @var string  */
    protected $country = '';

    protected $languages = [];

    /**
     * @param string $langset
     * @return void
     */
    public function switchLangSet(string $langset)
    {
        if (empty($langset)) {
            return;
        }
        if (empty($this->country)) {
            $this->setCountry($this->defaultCountry());
        }

        $this->setLangSet($langset);

        // Load system language pack
        $systemFile = $this->app->getThinkPath() . 'lang' . DIRECTORY_SEPARATOR . $langset . '.php';
        if (!file_exists($systemFile)) {
            $systemFile = $this->app->getThinkPath() . 'lang' . DIRECTORY_SEPARATOR . $this->defaultLangSet() . '.php';
            !file_exists($systemFile) && $systemFile = $this->app->getThinkPath() . 'lang' . DIRECTORY_SEPARATOR . 'en.php';
        }
        $this->load([$systemFile]);

        // Load system language pack
        $files = glob($this->app->getAppPath() . 'lang' . DIRECTORY_SEPARATOR . $langset . '.*');
        // Merge language packs for the corresponding country
        if (!empty($this->country)) {
            $files = array_merge($files, glob($this->app->getAppPath() . 'lang' . DIRECTORY_SEPARATOR . $this->country . DIRECTORY_SEPARATOR . $langset . '.*'));
        }
        $this->load($files);

        // Load custom language pack
        $list = $this->app->config->get('lang.extend_list', []);

        if (isset($list[$langset])) {
            $this->load($list[$langset]);
        }
        // Load custom language pack for the corresponding country
        if (!empty($this->country)) {
            if (isset($list[$this->country . '-' . $langset])) {
                $this->load($list[$this->country . '-' . $langset]);
            }
        }
    }

    /**
     * @param string $country
     * @param string $range
     * @return void
     */
    public function switchCountry(string $country, string $range = ''):void {
        if (empty($range)) $range = $this->getLangSet();

        $this->setCountry($country);

        $this->switchLangSet($range);
    }

    /**
     * Load language definition (case-insensitive)
     * @access public
     * @param string|array $file  Language file
     * @param string       $range Language scope
     * @return array
     */
    public function load($file, $range = ''): array
    {
        $range = $range ?: $this->getLangSet();
        if (!isset($this->languages[$this->country][$range])) {
            $this->languages[$this->country][$range] = [];
        }

        $lang = [];

        foreach ((array) $file as $name) {
            if (is_file($name)) {
                $result = $this->parse($name);
                $lang   = array_change_key_case($result) + $lang;
            }
        }

        if (!empty($lang)) {
            $this->languages[$this->country][$range] = $lang + $this->languages[$this->country][$range];
        }

        return $this->languages[$this->country][$range];
    }

    /**
     * @param string $country
     * @return void
     */
    public function setCountry(string $country):void {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCountry(): string {
        return $this->country;
    }

    /**
     * @return string
     */
    public function defaultCountry(): string {
        return $this->config['default_country'];
    }

    /**
     * Check if language definition exists (case-insensitive)
     * @access public
     * @param string|null $name  Language variable
     * @param string      $range Language scope
     * @return bool
     */
    public function has(string $name, string $range = ''): bool
    {
        $range = $range ?: $this->getLangSet();

        if ($this->config['allow_group'] && strpos($name, '.')) {
            [$name1, $name2] = explode('.', $name, 2);
            return isset($this->languages[$this->country][$range][strtolower($name1)][$name2]);
        }

        return isset($this->languages[$this->country][$range][strtolower($name)]);
    }

    /**
     * Retrieve language definition (case-insensitive)
     * @access public
     * @param string|null $name  Language variable
     * @param array       $vars  Variable substitution
     * @param string      $range Language scope
     * @return mixed
     */
    public function get(string $name = null, array $vars = [], string $range = '')
    {
        $range = $range ?: $this->getLangSet();

        if (!isset($this->languages[$this->country][$range])) {
            $this->switchLangSet($range);
        }

        // Empty parameter returns all definitions
        if (is_null($name)) {
            return $this->languages[$this->country][$range] ?? [];
        }

        if ($this->config['allow_group'] && strpos($name, '.')) {
            [$name1, $name2] = explode('.', $name, 2);

            $value = $this->languages[$this->country][$range][strtolower($name1)][$name2] ?? $name;
        } else {
            $value = $this->languages[$this->country][$range][strtolower($name)] ?? $name;
        }

        // Variable parsing
        if (!empty($vars) && is_array($vars)) {
            /**
             * Notes:
             * For ease of detection, the judgment of numeric indexing is simply that the key of the first element of the parameter array is the number 0.
             * Numeric indexing utilizes the system's sprintf function for replacement. Please refer to the sprintf function for usage.
             */
            if (key($vars) === 0) {
                // Numeric index parsing
                array_unshift($vars, $value);
                $value = call_user_func_array('sprintf', $vars);
            } else {
                // Associative index parsing
                $replace = array_keys($vars);
                foreach ($replace as &$v) {
                    $v = "{:{$v}}";
                }
                $value = str_replace($replace, $vars, $value);
            }
        }

        return $value;
    }

    public function replace(array $replace, string $range = ''): void
    {
        if (empty($range)) $range = $this->range;
        if (!isset($this->languages[$range])) {
            $this->switchLangSet($range);
        }

        if (!empty($replace['replace'])) {
            $this->languages[$range] = array_change_key_case($replace['replace']) + $this->languages[$range];
        }

        if (!empty($replace['suffix'])) {
            $replace['suffix'] = array_change_key_case($replace['suffix']);
            foreach ($replace['suffix'] as $key => $value) {
                if (isset($this->languages[$range][$key])) {
                    $this->languages[$range][$key] .= $value;
                }
            }
        }
    }
}