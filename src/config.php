<?php

return [
    // default language
    'default_lang'    => env('DEFAULT_LANG', 'en'),
    // Default country
    'default_country' => env('DEFAULT_LANG_COUNTRY', 'en'),
    // List of allowed languages
    'allow_lang_list' => [],
    // Multilingual automatic detection of variable names
    'detect_var'      => 'lang',
    // Whether to use cookies to record
    'use_cookie'      => false,
    // Multilingual cookie variables
    'cookie_var'      => 'think_lang',
    // Multilingual header variables
    'header_var'      => 'accept-language',
    // Extended Language Pack
    'extend_list'     => [
    ],
    // Accept Language is escaped as the corresponding language pack name
    'accept_language' => [],
    // Is language grouping supported
    'allow_group'     => true,
    // Language version
    'lang_version'    => '1'
];