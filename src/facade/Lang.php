<?php
declare(strict_types=1);

namespace markg\lang\facade;

/**
 * @see \markg\lang\Lang
 * @package markg\lang\facade
 * @mixin \markg\lang\Lang
 * @method static void setCountry(string $country) Set current country
 * @method static string getCountry() Retrieve current country
 * @method static void switchCountry(string $country, string $range = '') Switch current language && language
 * @method static void replace(array $replace, string $range = '') Copywriting replacement or splicing conversion
 */
class Lang extends \think\facade\Lang
{
    /**
     * @return string
     */
    protected static function getFacadeClass(): string
    {
        return \markg\lang\Lang::class;
    }
}